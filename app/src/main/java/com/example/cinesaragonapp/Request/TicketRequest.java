package com.example.cinesaragonapp.Request;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

public class TicketRequest extends StringRequest {
    private static final String ruta = "https://cinesaragonapp.000webhostapp.com/buy_tickets.php";
    private Map<String, String> parameters;
    /** NO FUNCIONA
    /**
     * Asocia un ticket a un usuario.
     * NO FUNCIONA
     * @param nameId
     * @param name
     * @param email
     * @param film
     * @param cinema
     * @param fecha
     * @param listener
     */
    public TicketRequest(String nameId, String name, String email, String film, String cinema,
            String fecha, Response.Listener<String> listener){
        super(Request.Method.POST, ruta, listener, null);
        parameters = new HashMap<>();
        parameters.put("nameid", nameId+"");
        parameters.put("name", name+"");
        parameters.put("email", email+"");
        parameters.put("film", film+"");
        parameters.put("cinema", cinema+"");
        parameters.put("fecha", fecha+"");

    }

    public Map<String, String>  getParams() {
        return  parameters;
    }
}
