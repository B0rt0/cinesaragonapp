package com.example.cinesaragonapp.Request;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

public class RegisterRequest extends StringRequest {
    private static final String ruta = "https://cinesaragonapp.000webhostapp.com/register.php";
    private Map<String, String> parameters;

    /**
     * Registra un nuevo usuario
     * @param name
     * @param email
     * @param password
     * @param listener
     */
    public RegisterRequest(String name, String email, String password, Response.Listener<String> listener){
        super(Request.Method.POST, ruta, listener, null);
        parameters = new HashMap<>();
        parameters.put("name", name+"");
        parameters.put("password", password+"");
        parameters.put("email", email+"");
    }

    public Map<String, String>  getParams() {
            return  parameters;
    }
}
