package com.example.cinesaragonapp.Request;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.Map;

public class CinesRequest extends StringRequest {
    private static final String ruta = "https://cinesaragonapp.000webhostapp.com/get_cinemas.php";

    /**
     * Devuelve un listado con todos los cines
     * @param listener
     */
    public CinesRequest(Response.Listener<String> listener) {
        super(Request.Method.POST, ruta, listener, null);
    }

    @Override
    protected Map<String, String> getParams() throws AuthFailureError {
        return super.getParams();
    }
}

