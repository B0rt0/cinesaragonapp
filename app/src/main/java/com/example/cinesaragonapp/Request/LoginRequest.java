package com.example.cinesaragonapp.Request;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

public class LoginRequest extends StringRequest {
    private static final String ruta = "https://cinesaragonapp.000webhostapp.com/login.php";
    private Map<String, String> parameters;

    /**
     * Envia una solicitud de login
     * @param nombre
     * @param password
     * @param listener
     */
    public LoginRequest(String nombre, String password, Response.Listener<String> listener) {
        super(Request.Method.POST, ruta, listener, null);
        parameters = new HashMap<>();
        parameters.put("user", nombre+"");
        parameters.put("password", password+"");

    }
    public Map<String, String> getParams() {
        return parameters;
    }
}