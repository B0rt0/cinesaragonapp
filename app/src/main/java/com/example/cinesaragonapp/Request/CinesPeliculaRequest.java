package com.example.cinesaragonapp.Request;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;


public class CinesPeliculaRequest extends StringRequest {
    private static final String ruta = "https://cinesaragonapp.000webhostapp.com/get_cinemas_with_film.php";
    private Map<String, String> parameters;

    /**
     * Devuelve un listado con los cines que contienen una pelicula
     * @param idPelicula
     * @param listener
     */
    public CinesPeliculaRequest(String idPelicula,  Response.Listener<String> listener) {
        super(Request.Method.POST, ruta, listener, null);
        parameters = new HashMap<>();
        parameters.put("filmid", idPelicula+"");


    }
    public Map<String, String> getParams() {
        return parameters;
    }
}
