package com.example.cinesaragonapp.Request;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

/**
 * Devuelve un listado de las peliculas que hay en el cine en JSON
 */
public class PeliculasCineRequest extends StringRequest {
    private static final String ruta = "https://cinesaragonapp.000webhostapp.com/get_films_in_cinema.php";
    private Map<String, String> parameters;

    /**
     * Devuelve todas las peliculas que hay en un cine
     * @param idCine
     * @param listener
     */
    public PeliculasCineRequest(String idCine,  Response.Listener<String> listener) {
        super(Request.Method.POST, ruta, listener, null);
        parameters = new HashMap<>();
        parameters.put("cinemaid", idCine+"");


    }
    public Map<String, String> getParams() {
        return parameters;
    }
}
