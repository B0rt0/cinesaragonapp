package com.example.cinesaragonapp.Activitys;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;
import com.example.cinesaragonapp.R;
import com.example.cinesaragonapp.Request.LoginRequest;

import org.json.JSONException;
import org.json.JSONObject;

public class Login extends AppCompatActivity {
    EditText etUser;
    EditText etPassword;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        TextView registro = (TextView)findViewById(R.id.tvSingin);
        Button btnLogin = (Button)findViewById(R.id.btnConnect);
        etUser = (EditText)findViewById(R.id.tbName);
        etPassword = (EditText)findViewById(R.id.tbPass);

    }

    /**
     * Nos envia a la actividad de registro
     * @param view
     */
    public void GoRegister(View view) {
        Intent registro = new Intent(Login.this, Registro.class);
        Login.this.startActivity(registro);
        finish();
    }

    /**
     * Trata de hace login
     * @param view
     */
    public void connect(View view) {
        final String user = etUser.getText().toString();
        final String pass = etPassword.getText().toString();
        Response.Listener<String> response = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try{
                    JSONObject jsonResponse = new JSONObject(response);
                    boolean ok = jsonResponse.getBoolean("success");
                    if(ok==true){

                        String nombre = jsonResponse.getString("nombre");
                        String email = jsonResponse.getString("email");
                        int id = jsonResponse.getInt("id");
                        Intent inicio = new Intent(Login.this, Inicio.class);
                        Inicio.login=true;
                        Inicio.emailUst = email;
                        Inicio.nameUsr=nombre;
                        Inicio.idUsr =id;
                        Login.this.startActivity(inicio);
                        Login.this.finish();
                    }else{{
                        AlertDialog.Builder alerta = new AlertDialog.Builder(Login.this);
                        alerta.setMessage("Fallo en el login").setNegativeButton("Reintentar", null).create().show();
                    }}
                }catch (JSONException e){
                    e.getMessage();
                }
            }
        };

        LoginRequest r = new LoginRequest(user, pass, response);
        RequestQueue cola = Volley.newRequestQueue(Login.this);
        cola.add(r);
    }
}
