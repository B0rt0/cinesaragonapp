package com.example.cinesaragonapp.Activitys;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;
import com.example.cinesaragonapp.R;
import com.example.cinesaragonapp.Request.RegisterRequest;

import org.json.JSONException;
import org.json.JSONObject;

public class Registro extends AppCompatActivity {
    EditText name;
    EditText email;
    EditText pass;
    EditText pass2;
    CheckBox showPass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);
        name = (EditText)findViewById(R.id.tbName);
        email = (EditText)findViewById(R.id.tbEmail);
        pass = (EditText)findViewById(R.id.tbPass);
        pass2 = (EditText)findViewById(R.id.tb2ndPass);
        showPass = (CheckBox) findViewById(R.id.cbShowPass);

        /**
         *Comprueba si se ha cambiado el estado del CheckBox y oculta o muestra las contraseñas
         */

        showPass.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    pass.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    pass2.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                }else{
                    pass.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    pass2.setTransformationMethod(PasswordTransformationMethod.getInstance());
                }
            }
        });
    }

    /**
     * Crea el nuevo usuario
     * @param view
     */
    public void CreateUser(View view) {
        if(checkPass()){
            String nombre = name.getText().toString();
            String correo = email.getText().toString();
            String contra = pass.getText().toString();

            Response.Listener<String> respuesta = new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try{
                        JSONObject jsonAnswer = new JSONObject(response);
                        boolean ok = jsonAnswer.getBoolean("success");
                        if (ok == true) {
                            Intent i = new Intent(Registro.this, Login.class);
                            Registro.this.startActivity(i);
                            Registro.this.finish();
                        }else{
                            AlertDialog.Builder alerta = new AlertDialog.Builder(Registro.this);
                            alerta.setMessage("Fallo en el registro").setNegativeButton("Reintentar", null).create().show();
                        }
                    }catch (JSONException e)
                    {
                        e.getMessage();
                    }
                }
            };

            RegisterRequest r = new RegisterRequest(nombre, correo, contra, respuesta);
            RequestQueue cola = Volley.newRequestQueue(Registro.this);
            cola.add(r);
        }else{
            Toast.makeText(this, "Ambas contraseñas deben coincidir.", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Comprueba que ambas pass coinciden.
     * @return
     */
    private boolean checkPass() {
        String p1 = pass.getText().toString();
        String p2 = pass2.getText().toString();
        if(p1.equals(p2)){
            return true;
        } else {
            return false;
        }
    }
}

