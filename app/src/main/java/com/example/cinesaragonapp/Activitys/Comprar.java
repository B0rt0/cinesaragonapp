package com.example.cinesaragonapp.Activitys;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.telephony.mbms.MbmsErrors;
import android.view.View;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;
import com.example.cinesaragonapp.R;
import com.example.cinesaragonapp.Request.TicketRequest;
import com.example.cinesaragonapp.clases.Cine;
import com.example.cinesaragonapp.clases.Pelicula;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;

import javax.microedition.khronos.egl.EGLDisplay;

public class Comprar extends AppCompatActivity {

    //Visual
    private ImageView imagen;
    private TextView titulo;
    private EditText usuario;
    private EditText email;
    private EditText peliculaName;
    private EditText cineName;
    private CalendarView calendario;
    private Button comprar;

    private Cine cine;
    int idCine;
    private Pelicula pelicula;
    int idPelicula;

    public String fechaCalendario ="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comprar);

        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();
        /**
         * Inicializamos los componentes
         */
        initComponents();
        /**
         * Cargamos los datos de la entrada.
         */
        loadTicket();
        /**
         * Damos valor a los componentes.
         */
        valueComponents();
        /**
         * Toma la fecha del calendario
         */
        calendario.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(@NonNull CalendarView calendarView, int y, int m, int d) {
                fechaCalendario=d+"/"+m+"/"+y;
            }
        });
    }

    /**
     * Da valores a los componentes visuales
     */
    private void valueComponents() {
       try{
           Picasso.get().load(pelicula.imagen).into(imagen);
       }catch (NullPointerException e){
           Toast.makeText(Comprar.this,"Se ha producido un error durante la carga de la imagen.",Toast.LENGTH_LONG).show();
       }

        titulo.setText(pelicula.nombre);
        usuario.setText(Inicio.nameUsr);
        email.setText(Inicio.emailUst);
        peliculaName.setText(pelicula.nombre);
        cineName.setText(cine.nombre);
    }

    /**
     * Cargamos nuestros objetos cine y pelicula para la entrada
     */
    private void loadTicket() {
        Intent intent = getIntent();
        idCine= intent.getIntExtra("idCine", 0);
        idPelicula= intent.getIntExtra("idPelicula", 0);

        for(int n = 0; n < Inicio.cines.size(); n++){
            if(Inicio.cines.get(n).getId() ==idCine ){
               cine = Inicio.cines.get(n);
            }
        }
        for(int n = 0; n < Inicio.peliculas.size(); n++){
            if(Inicio.peliculas.get(n).getId() ==idPelicula ){
                pelicula = Inicio.peliculas.get(n);
            }
        }
    }

    /**
     * Inicializamos los componentes
     */
    private void initComponents() {
        imagen = findViewById(R.id.ivImage);
        titulo = findViewById(R.id.tvTitulo);
        usuario = findViewById(R.id.edNombre);
        email = findViewById(R.id.edEmail);
        peliculaName = findViewById(R.id.edPelicula);
        cineName = findViewById(R.id.edCine);
        calendario = findViewById(R.id.cvCalendario);
        comprar = findViewById(R.id.btnComprar);
    }
    /** NO FUNCIONA
    /**
     * Realiza la compra de un ticket
     * @param view
     */
    public void buyTicket(View view) {
        Response.Listener<String> response = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try{
                    JSONObject jsonAnswer = new JSONObject(response);
                    boolean ok = jsonAnswer.getBoolean("success");
                    if(ok){
                        Toast.makeText(Comprar.this,"La compra se ha realizado correctamente.",Toast.LENGTH_LONG).show();
                    }else{
                        Toast.makeText(Comprar.this,"Se ha producido un error durante la compra.",Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };
       TicketRequest r = new TicketRequest(
               Integer.toString(Inicio.idUsr),
               usuario.getText().toString(),
               email.getText().toString(),
               peliculaName.getText().toString(),
               cineName.getText().toString(),
               fechaCalendario,
               response);
        RequestQueue cola = Volley.newRequestQueue(Comprar.this);
        cola.add(r);
    }

}
