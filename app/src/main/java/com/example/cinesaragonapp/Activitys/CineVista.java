package com.example.cinesaragonapp.Activitys;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;
import com.example.cinesaragonapp.R;
import com.example.cinesaragonapp.Request.PeliculasCineRequest;
import com.example.cinesaragonapp.adaptadores.AdaptadorPeliculas;
import com.example.cinesaragonapp.clases.Cine;
import com.example.cinesaragonapp.clases.Pelicula;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class CineVista extends AppCompatActivity {

    //Controles
    private Cine cine;
    private ImageView imagen;
    private TextView nombre;
    private TextView detalle;
    private TextView descripccion;
    private ListView lvItems;
    private TextView tvLista;

    //Lista de peliculas
    public static String urlImagenes = "https://cinesaragonapp.000webhostapp.com/img/";
    ArrayList<Pelicula> peliculas = new ArrayList();

    //Identificador del cine
    public static int id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cine_vista);

        /**
         * Cargamos los datos del intent
         */
        loadCinema();
        /**
         * Cargamos las pelicuals que hay en el cine
         */
        loadFilmsInCinema();
        /**
         * Iniciamos componenters
         */
        initComponents();
        /**
         * Damos valor a los components
         */
        valueComponents();
    }


    /**
     * Cargamos las peliculas que hay en el cine mediante WS
     */
    private void loadFilmsInCinema() {
        Response.Listener<String> response = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray jArray = new JSONArray(response);
                    for (int i = 0; i < jArray.length(); i++) {
                        JSONObject pelicula = jArray.getJSONObject(i);
                        Pelicula peli = new Pelicula();
                        peli.setId(pelicula.getInt("id"));
                        peli.setNombre(pelicula.getString("name"));
                        peli.setDescripccion(pelicula.getString("description"));
                        peli.setDirector(pelicula.getString("direction"));
                        peli.setFecha(pelicula.getString("date"));

                        peli.setImagen(urlImagenes+ pelicula.getString("image"));
                        peliculas.add(peli);
                    }
                    AdaptadorPeliculas adapter = new AdaptadorPeliculas(getApplicationContext(), peliculas);
                    lvItems.setAdapter(adapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };

        PeliculasCineRequest r = new PeliculasCineRequest(Integer.toString(cine.id), response);
        RequestQueue cola = Volley.newRequestQueue(CineVista.this);
        cola.add(r);
    }

    /**
     * Cargamos los datos del intent buscando el id del cine
     */
    private void loadCinema() {
        Intent intent = getIntent();
        id = intent.getIntExtra("idCine", 0);

        for(int n = 0; n < Inicio.cines.size(); n++){
            if(Inicio.cines.get(n).getId() ==id ){
                cine = Inicio.cines.get(n);

            }
        }
    }

    /**
    * Iniciamos los componentes visuales
    * */
    private void initComponents() {
        imagen = findViewById(R.id.ivImage);
        nombre = findViewById(R.id.tvName);
        detalle = findViewById(R.id.tvDetails);
        descripccion = findViewById(R.id.tvDescription);
        lvItems = findViewById(R.id.lvElements);
        tvLista = findViewById(R.id.tvListado);
    }

    /**
     * Damos valor a los componentes
     */
    private void valueComponents() {
        Picasso.get().load(cine.imagen).into(imagen);
        nombre.setText(cine.nombre);
        detalle.setText(cine.dieccion);
        descripccion.setText(cine.descripccion);
        tvLista.setText("Cartelera:");
        descripccion.setMovementMethod(new ScrollingMovementMethod());
    }
}
