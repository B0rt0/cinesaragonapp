package com.example.cinesaragonapp.Activitys;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;
import com.example.cinesaragonapp.R;
import com.example.cinesaragonapp.Request.CinesPeliculaRequest;
import com.example.cinesaragonapp.Request.PeliculasCineRequest;
import com.example.cinesaragonapp.adaptadores.AdaptadorCines;
import com.example.cinesaragonapp.adaptadores.AdaptadorPeliculas;
import com.example.cinesaragonapp.clases.Cine;
import com.example.cinesaragonapp.clases.Pelicula;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class PeliculaVista extends AppCompatActivity {
    //Controles
    private Pelicula pelicula;
    private ImageView imagen;
    private TextView nombre;
    private TextView detalle;
    private TextView descripccion;
    private ListView lvItems;
    private TextView tvLista;

    //Lista de peliculas
    public static String urlImagenes = "https://cinesaragonapp.000webhostapp.com/img/";
    ArrayList<Cine> cines = new ArrayList();

    //Identificador de la pelicula
    public static int id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pelicula_vista);
        /**
         * Cargamos los datos del intent
         */
        loadFilm();
        /**
         * Cargamos los cines con la pelicula
         */
        loadCinemasWithFilm();
        /**
         * Iniciamos componenters
         */
        initComponents();
        /**
         * Damos valor a los components
         */
        valueComponents();
    }
    /**
     * Cargamos las peliculas que hay en el cine mediante WS
     */
    private void loadCinemasWithFilm() {
        Response.Listener<String> response = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray jArray = new JSONArray(response);
                    for (int i = 0; i < jArray.length(); i++) {
                        JSONObject cinema = jArray.getJSONObject(i);
                        Cine c = new Cine();
                        c.setId(cinema.getInt("id"));
                        c.setNombre(cinema.getString("name"));
                        c.setDescripccion(cinema.getString("description"));
                        c.setDieccion(cinema.getString("address"));
                        c.setImagen(urlImagenes+ cinema.getString("image"));
                        cines.add(c);
                    }
                    AdaptadorCines adapter = new AdaptadorCines(getApplicationContext(), cines);
                    lvItems.setAdapter(adapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };

        CinesPeliculaRequest r = new CinesPeliculaRequest(Integer.toString(pelicula.id), response);
        RequestQueue cola = Volley.newRequestQueue(PeliculaVista.this);
        cola.add(r);
    }

    /**
     * Cargamos los datos del intent buscando el id del cine
     */
    private void loadFilm() {
        Intent intent = getIntent();
        id = intent.getIntExtra("idPelicula", 0);

        for(int n = 0; n < Cartelera.peliculas.size(); n++){
            if(Cartelera.peliculas.get(n).getId() ==id ){
                pelicula = Cartelera.peliculas.get(n);
            }
        }
    }

    /**
     * Iniciamos los componentes visuales
     * */
    private void initComponents() {
        imagen = findViewById(R.id.ivImage);
        nombre = findViewById(R.id.tvName);
        detalle = findViewById(R.id.tvDetails);
        descripccion = findViewById(R.id.tvDescription);
        lvItems = findViewById(R.id.lvElements);
        tvLista = findViewById(R.id.tvListado);
    }

    /**
     * Damos valor a los componentes
     */
    private void valueComponents() {
        Picasso.get().load(pelicula.imagen).into(imagen);
        nombre.setText(pelicula.nombre);
        detalle.setText(pelicula.fecha + " - " + pelicula.getDirector());
        descripccion.setText(pelicula.descripccion);
        tvLista.setText("Disponible en:");
        descripccion.setMovementMethod(new ScrollingMovementMethod());
    }
}
