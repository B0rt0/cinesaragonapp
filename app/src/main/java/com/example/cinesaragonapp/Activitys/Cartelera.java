package com.example.cinesaragonapp.Activitys;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ListView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;
import com.example.cinesaragonapp.R;
import com.example.cinesaragonapp.Request.PeliculasRequest;
import com.example.cinesaragonapp.adaptadores.AdaptadorPeliculas;
import com.example.cinesaragonapp.clases.Pelicula;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;

public class Cartelera extends AppCompatActivity {

    public static String urlImagenes = "https://cinesaragonapp.000webhostapp.com/img/";
    public static ArrayList<Pelicula> peliculas = new ArrayList();

    private AdaptadorPeliculas adaptadorPeliculas;
    private ListView listaPelicualas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cartelera);

        /**
         * Establece el modo cartelera de AdaptadorPeliculas
         */
        Inicio.enCartelera = true;

        /**
         * Carga la lista de pelicualas
         */
        cargarPeliculas();

        /**
         * Iniciamos los componentes
         */
        initComponents();

    }

    /**
     * Inicia los componentes
     */
    private void initComponents() {
        listaPelicualas = findViewById(R.id.lvInicio);
    }

    /**
     * Devuelve el contexto de la aplicación
     * @return Context
     */
    @Override
    public Context getApplicationContext() {
        return super.getApplicationContext();
    }

    /**
     * Carga todas las pelicuals recibidas en un en un listado de objetos Pelicula
     */
    public void cargarPeliculas() {

        Response.Listener<String> response = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray jArray = new JSONArray(response);
                    for (int i = 0; i < jArray.length(); i++) {
                        JSONObject pelicula = jArray.getJSONObject(i);
                        Pelicula peli = new Pelicula();
                        peli.setId(pelicula.getInt("id"));
                        peli.setNombre(pelicula.getString("name"));
                        peli.setDescripccion(pelicula.getString("description"));
                        peli.setDirector(pelicula.getString("direction"));
                        peli.setFecha(pelicula.getString("date"));

                        peli.setImagen(urlImagenes+ pelicula.getString("image"));
                        peliculas.add(peli);
                    }
                     AdaptadorPeliculas adapter = new AdaptadorPeliculas(getApplicationContext(), peliculas);
                    listaPelicualas.setAdapter(adapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };

        PeliculasRequest r = new PeliculasRequest(response);
        RequestQueue cola = Volley.newRequestQueue(Cartelera.this);
        cola.add(r);
    }

    @Override
    public void onBackPressed() {

        Intent intent = new Intent(this, Inicio.class);
        this.startActivity(intent);
        Inicio.enCartelera = false;
        Cartelera.this.finish();
    }

}

