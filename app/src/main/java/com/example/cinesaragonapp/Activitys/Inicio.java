package com.example.cinesaragonapp.Activitys;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;
import com.example.cinesaragonapp.R;
import com.example.cinesaragonapp.Request.CinesRequest;
import com.example.cinesaragonapp.Request.PeliculasRequest;
import com.example.cinesaragonapp.adaptadores.AdaptadorCines;
import com.example.cinesaragonapp.adaptadores.AdaptadorPeliculas;
import com.example.cinesaragonapp.clases.Cine;
import com.example.cinesaragonapp.clases.Pelicula;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Inicio extends AppCompatActivity {

    public static String urlImagenes = "https://cinesaragonapp.000webhostapp.com/img/";
    public static ArrayList<Cine> cines = new ArrayList();
    public static ArrayList<Pelicula> peliculas = new ArrayList<>();
    private AdaptadorCines adaptadorCines;
    private ListView listaCines;

    public static Boolean cinesCargados = false;

    //Control de usuario
    public static Boolean login = false;
    public static Boolean enCartelera = false;
    public static String emailUst;
    public static int idUsr;
    public static String nameUsr;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inicio);
        /**
         * Carga el array de cines y los muestra en el list view
         */
        cargarCines();
        /**
         * Cargamos las peliculas para poderlas usar mas adelante
         */
        cargarPeliculas();
        /**
         * Iniciamos componentes
         */
        initComponents();
    }

    private void initComponents() {listaCines = findViewById(R.id.lvInicio);}

    /**
     * Devuelve el contexto de la aplicación
     * @return Context
     */
    public Context getApplicationContext() {
        return super.getApplicationContext();
    }

    /**
     * Carga todos los cines y los recibe en un en un listado de objetos Cine
     */
    private void cargarCines() {
        Response.Listener<String> response = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try { JSONArray jArray = new JSONArray(response);
                        if(!cinesCargados) {
                            for (int i = 0; i < jArray.length(); i++) {
                                JSONObject cine = jArray.getJSONObject(i);
                                Cine cinema = new Cine();
                                cinema.setId(cine.getInt("id"));
                                cinema.setNombre(cine.getString("name"));
                                cinema.setDescripccion(cine.getString("description"));
                                cinema.setDieccion(cine.getString("address"));

                                cinema.setImagen(urlImagenes + cine.getString("image"));
                                cines.add(cinema);
                            }
                        }
                        AdaptadorCines adapter = new AdaptadorCines(getApplicationContext(), cines);
                        listaCines.setAdapter(adapter);
                        cinesCargados = true;


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };

        CinesRequest r = new CinesRequest(response);
        RequestQueue cola = Volley.newRequestQueue(Inicio.this);
        cola.add(r);
    }

    /**
     * Cargamos las peliculas para usarlas posteriormente
     */
    public void cargarPeliculas() {

        Response.Listener<String> response = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray jArray = new JSONArray(response);
                    if (!cinesCargados) {
                        for (int i = 0; i < jArray.length(); i++) {
                            JSONObject pelicula = jArray.getJSONObject(i);
                            Pelicula peli = new Pelicula();
                            peli.setId(pelicula.getInt("id"));
                            peli.setNombre(pelicula.getString("name"));
                            peli.setDescripccion(pelicula.getString("description"));
                            peli.setDirector(pelicula.getString("direction"));
                            peli.setFecha(pelicula.getString("date"));

                            peli.setImagen(urlImagenes + pelicula.getString("image"));
                            peliculas.add(peli);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };
        PeliculasRequest r = new PeliculasRequest(response);
        RequestQueue cola = Volley.newRequestQueue(Inicio.this);
        cola.add(r);
    }

    /**
     * Insertamos nuestro menu
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        if(login){
            inflater.inflate(R.menu.menu_login, menu);
            return true;

        }else{
            inflater.inflate(R.menu.menu_basico, menu);
            return true;
        }
    }
    /**
     * Damos funcionalidad a nuestro menu
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.login:
                Intent a = new Intent(Inicio.this, Login.class);
                Inicio.this.startActivity(a);
                Inicio.this.finish();
                return true;
            case R.id.register:
                Intent b = new Intent(Inicio.this, Registro.class);
                Inicio.this.startActivity(b);
                Inicio.this.finish();
                return true;
            case R.id.cartelera:
                Intent c = new Intent(Inicio.this, Cartelera.class);
                Inicio.this.startActivity(c);
                Inicio.this.finish();
                return true;
            case R.id.logout:
                Intent d = new Intent(Inicio.this, Inicio.class);
                Inicio.login=false;
                Toast.makeText(this, "Hasta pronto " + Inicio.nameUsr, Toast.LENGTH_LONG).show();
                Inicio.this.startActivity(d);

                Inicio.this.finish();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }
}
