package com.example.cinesaragonapp.adaptadores;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.example.cinesaragonapp.Activitys.CineVista;
import com.example.cinesaragonapp.R;
import com.example.cinesaragonapp.clases.Cine;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class AdaptadorCines extends ArrayAdapter<Cine> {

    private Context context;
    private ArrayList<Cine> lstCines;


    public AdaptadorCines(Context context,
                          ArrayList<Cine> lstCines) {
        super(context, R.layout.item_line, lstCines);
        this.context = context;
        this.lstCines = lstCines;
    }

    @NonNull
    @Override
    public View getView(int position,
                        View filaVisual,
                        ViewGroup parent) {
        final Cine cine = lstCines.get(position);
        if (filaVisual == null) {
            //Primera vez que se construye la fila
            filaVisual = LayoutInflater.from(context).inflate(R.layout.item_line, null);
            ImageView imagen = filaVisual.findViewById(R.id.image);
            TextView tvNombre = filaVisual.findViewById(R.id.name);
            TextView tvDetalle = filaVisual.findViewById(R.id.detail);

            Picasso.get().load(cine.imagen).into(imagen);
            tvNombre.setText(cine.nombre);
            tvDetalle.setText(cine.dieccion);
            final int id = cine.id;

            filaVisual.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, CineVista.class);
                    intent.putExtra("idCine", id);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                }
            });
        }

        return filaVisual;
    }
}
