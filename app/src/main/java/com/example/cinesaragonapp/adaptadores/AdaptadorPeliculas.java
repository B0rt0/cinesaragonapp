package com.example.cinesaragonapp.adaptadores;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.example.cinesaragonapp.Activitys.Cartelera;
import com.example.cinesaragonapp.Activitys.CineVista;
import com.example.cinesaragonapp.Activitys.Comprar;
import com.example.cinesaragonapp.Activitys.Inicio;
import com.example.cinesaragonapp.Activitys.PeliculaVista;
import com.example.cinesaragonapp.R;
import com.example.cinesaragonapp.clases.Cine;
import com.example.cinesaragonapp.clases.Pelicula;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;


public class AdaptadorPeliculas extends ArrayAdapter<Pelicula> {

    private Context context;
    private ArrayList<Pelicula>lstPeliculas;
    public AdaptadorPeliculas(Context context,
                              ArrayList<Pelicula> lstPeliculas) {
        super(context, R.layout.item_line, lstPeliculas);
        this.context=context;
        this.lstPeliculas=lstPeliculas;
    }

    @NonNull
    @Override
    public View getView(int position,
                        View filaVisual,
                        ViewGroup parent) {
        Pelicula pelicula = lstPeliculas.get(position);
        if(filaVisual==null){
            //Primera vez que se construye la fila
            filaVisual= LayoutInflater.from(context).inflate(R.layout.item_line, null);
            ImageView imagen = filaVisual.findViewById(R.id.image);
            TextView  tvNombre = filaVisual.findViewById(R.id.name);
            TextView  tvDetalle = filaVisual.findViewById(R.id.detail);

            Picasso.get().load(pelicula.imagen).into(imagen);
            tvNombre.setText(pelicula.nombre);
            tvDetalle.setText(pelicula.fecha + " " + pelicula.director);
            final int id = pelicula.id;
            filaVisual.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (Inicio.enCartelera) {
                        Intent intent = new Intent(context, PeliculaVista.class);
                        intent.putExtra("idPelicula", id);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(intent);
                    } else {
                        if (Inicio.login) {
                            Intent intent = new Intent(context, Comprar.class);
                            intent.putExtra("idPelicula", id);
                            intent.putExtra("idCine", CineVista.id);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            context.startActivity(intent);
                        } else {
                            Toast.makeText(context, "Debes registrarte para poder comprar tickets.", Toast.LENGTH_LONG).show();

                        }
                    }
                }

            });
        }

        return filaVisual;
    }
}